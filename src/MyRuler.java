import java.awt.Point;
import java.util.*;
import com.ibm.ruler.*;

public class MyRuler extends Ruler {
	
	private static final int MAX_PEASANTS = 50;
	private static final int MEAN_PEASANTS = 5;
	private static final int MIN_SUBJECTS = 3;
	private static final int PACK_SIZE = 8;
	private static final double PERCENT_CASTLE = .35;
	private static final int DANGER_ZONE = 10;
	
	public boolean allLandOwned = false;
	private ICastle startingCastle;
	private Map<Integer,WayPoint> positions = new HashMap<Integer,WayPoint>();
	
	protected Map<Integer,KnightGroup> castleGroupMap = new HashMap<Integer,KnightGroup>();
	private Set<Integer> assignedSubject;
	private Set<Integer> enemySet = new HashSet<Integer>();
	private List<KnightGroup> knightGroups;
	private List<PeasantGroup> peasantGroups;
	protected WayPoint middleOfMap;
	private int peasantSize;
	private int knightSize;
	private int quadrantSize;
	private int groupId = 400;
	private boolean assimilation = false;
	private Map<Integer, int[]> directionMap;
	private Map<String, WayPoint> quadrants; 
	//private Map<Integer, PositionTracker> trackMap = new HashMap<Integer, PositionTracker>();
	public String getRulerName() {
	  return "Herbie Hancock";
	}

	public String getSchoolName() {
	  return "You can put six packs of be.. soda in there.";
	}

	public void initialize() 
	{
		peasantSize=1;
		knightSize=5;
		quadrantSize=2;
		
		assignedSubject = new HashSet<Integer>();
		knightGroups = new ArrayList<KnightGroup>();
		peasantGroups = new ArrayList<PeasantGroup>();
		IRuler[] rulers = World.getOtherRulers();
		for(IRuler ruler:rulers)
		{
			System.out.println("Attention: "+ruler.getName()+"!!!!!");
		}
		System.out.println("Just like the bad guy from Lethal Weapon 2, I've got diplomatic immunity");
		
		directionMap = new HashMap<Integer,int[]>();
		directionMap.put(1,new int[]{0,-1});
		directionMap.put(2,new int[]{1,-1});
		directionMap.put(3,new int[]{1,0});
		directionMap.put(4,new int[]{1,1});
		directionMap.put(5,new int[]{0,1});
		directionMap.put(6,new int[]{-1,1});
		directionMap.put(7,new int[]{-1,0});
		directionMap.put(8,new int[]{-1,-1});
		
		
		startingCastle = getCastles()[0];
		positions.put(0,new WayPoint(0,0,"NorthWest",4));		
		positions.put(1,new WayPoint(World.WIDTH/2,0,"North",4));
		positions.put(2,new WayPoint(World.WIDTH-1,0,"NorthEast",4));
		positions.put(3,new WayPoint(World.WIDTH-1,World.HEIGHT/2,"East",4));
		positions.put(4,new WayPoint(World.WIDTH-1,World.HEIGHT-1,"SouthEast",4));
		positions.put(5,new WayPoint(World.WIDTH/2,World.HEIGHT-1,"South",4));
		positions.put(6,new WayPoint(0,World.HEIGHT-1,"SouthWest",4));
		positions.put(7,new WayPoint(0,World.HEIGHT/2,"West",4));
		                      			   
		createQuadrants(quadrantSize); 
		
		middleOfMap = new WayPoint(World.WIDTH/2,World.HEIGHT/2,"Middle Of the World", 5);
		

	}
	protected Random rand = new Random(); 
	protected List<WayPoint> enemyPoints = new ArrayList<WayPoint>();

    protected int executeTime = 100;
    private boolean createPeasants;
	
	private int totalSize;
	private Integer castleValue;
	private boolean allIn;
    protected MyRuler getMyRuler()
    {
    	return this;
    }
    public void orderSubjects(int lastMoveTime) 
    {
		try {
			if(lastMoveTime>400)
			{
				System.out.println("Ohh.... Fudge.. ");
			}
			createDecision();
			setUpMoves();
			
			assignPeasants();
			assignKnights();
			// Don't do this unless your debugging.
			//checkForTrouble();
			
			if(World.getCurrentTurn()%50 == 0)
			{
				int store = World.getCurrentTurn();
				if(store>400)
				{
					boolean iAmGreat = true;
					int myPoints = getPoints();
					for(IRuler ruler: World.getOtherRulers())
					{
						if(ruler.getPoints()>myPoints)
						{
							iAmGreat = false;
						}
					}
					if(iAmGreat)
					{
						System.out.println("Victory is most certainly mine");
					}
					else
					{
						System.out.println("Spanky, you have failed me for the last time");
					}
				}
			}
			
			for(int i=0;i<peasantGroups.size();i++)
			{
				peasantGroups.get(i).setStrategy(new PeasantStrategy(this));
				peasantGroups.get(i).runStrategy();    		    		
			}
			List<KnightGroup> orderKnights = new ArrayList<KnightGroup>();
			orderKnights.addAll(knightGroups);			
			orderKnights.addAll(castleGroupMap.values());
			determineKnightStrategy(orderKnights.toArray(new KnightGroup[0]));
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    	
    }
    private void determineKnightStrategy(KnightGroup[] array)
    {
    	for(KnightGroup group:array)
    	{
    		TheWeakestLink strat = new TheWeakestLink(this);
    		group.setStrategy(strat);
    		group.runStrategy();
    	}

    }

	private void createDecision() 
	{
		if(getKnights().length<MIN_SUBJECTS)
		{
			createKnights();
		}
		else
		{
			if(!moreKnightsThanEnemies())
			{
				createKnights();
			}
			else
			{
				if (peasantGroups.size()<=MAX_PEASANTS  && !assimilation) 
		    	{
					if (peasantGroups.size() > MEAN_PEASANTS) {
						createPeasants = false;
					}
					if (peasantGroups.size() < MIN_SUBJECTS || createPeasants) {
						createPeasants();
						createPeasants = true;
					} else {
						if (doubleKnightsThanEnemies()) {
							createPeasants();
						} else {
							createKnights();
						}

					}
				}
		    	else
		    	{
		    		createKnights();
		    	}
			}
		}
    	
    	int totalCastles = World.getOtherCastles().length+getCastles().length;
    	double percentOwned = getCastles().length/totalCastles;
		if(percentOwned<PERCENT_CASTLE)
		{
			castleValue=-300;
		}
		else
		{
			castleValue = null;
		}
	}

	public boolean moreKnightsThanEnemies() {
    	if(getKnights().length>World.getOtherKnights().length && getKnights().length>2)
    	{
    		return true;
    	}
    	
    	return false;
	}
	
	public boolean doubleKnightsThanEnemies()
	{
		int mine = getKnights().length;
		int theirs = World.getOtherKnights().length*2;
		return mine>=theirs;
	}

	public void createKnights()
    {
		allIn = false;
    	for(ICastle castle: getCastles())
    	{
    		createKnights(castle);
    	}
    }
    
    public void createPeasants()
    {
    	allIn = true;
    	for(ICastle castle: getCastles())
    	{
    		createPeasants(castle);
    	}
    }
    
    public void assignPeasants()
    {
    	List<IPeasant> peasantList = new ArrayList<IPeasant>();
    	IPeasant[] peasants = getPeasants();
    	for(IPeasant peasant:peasants)
    	{
    		if(!assignedSubject.contains(peasant.getId()))
    		{
    			peasantList.add(peasant);
    			//trackMap.put(peasant.getId(), new PositionTracker(peasant));
    		}
    	}
    	List<PeasantGroup> groupList = new ArrayList<PeasantGroup>();
    	for(int i=0;i<peasantGroups.size();i++)
    	{
    		PeasantGroup peasantGroup = peasantGroups.get(i);
    		if(!peasantGroup.isDead())
    		{
    			if(!peasantGroup.isInAction())
    			{
    				while(!peasantGroup.isGroupFull() && peasantList.size()>0)
    				{
    					IPeasant peasant = peasantList.remove(0);
    					peasantGroup.addAGroupMember(peasant);
    				}
    			}
    			groupList.add(peasantGroup);
    		}
    	}
    	while(peasantList.size()>0)
    	{
    		PeasantGroup peasantGroup = new PeasantGroup(peasantSize);
    		while(!peasantGroup.isGroupFull() && peasantList.size()>0)
    		{
    			IPeasant peasant = peasantList.remove(0);
				peasantGroup.addAGroupMember(peasant);				
    		}
    		groupList.add(peasantGroup);
    	}
    	peasantGroups = groupList;    	
    }
    
//    public void checkForTrouble()	
//    {
//    	
//    	PositionTracker[] list=trackMap.values().toArray(new PositionTracker[0]);
//    	//PositionTracker[] list =posTrackList.toArray(new PositionTracker[0]);
//    	//posTrackList.clear();
//    	trackMap.clear();
//    	for(PositionTracker track: list)
//    	{
//    		if(track.getTrkdSubject().isAlive())
//    		{
//    			if(track.isConcern())
//    			{
//    				System.out.println("We Got Problems..subject "+track.getTrkdSubject().getId()+" IS not moving");
//    			}
//    			//posTrackList.add(track);
//    			trackMap.put(track.getTrkdSubject().getId(), track);
//    		}    		
//    	}
//    }
    
    public void assignKnights()
    {
		for(int j=0;j<knightGroups.size();j++)			
    	{
    		KnightGroup knightGroup = knightGroups.get(j);
    		if(knightGroup.isDead())
    		{
    			knightGroups.remove(j);
    		}
    	}
		for(Iterator<KnightGroup> it = castleGroupMap.values().iterator();it.hasNext();)			
    	{
    		KnightGroup knightGroup = it.next();
    		knightGroup.isDead();
    	}
    	List<IKnight> knightList = new ArrayList<IKnight>();
    	IKnight[] knights = getKnights();
    	for(IKnight knight:knights)
    	{
    		if(!assignedSubject.contains(knight.getId()))
    		{
    			knightList.add(knight);
    			//trackMap.put(knight.getId(), new PositionTracker(knight));
    			//posTrackList.add(new PositionTracker(knight));
    		}    		
    	}
    	List<KnightGroup> groupList = new ArrayList<KnightGroup>();
    	if(getCastles().length>0)
    	{
    		groupList.addAll(findCastleGroupAndAssign(knightList));
    	}
    	else
    	{    		
        	for(int i=0;i<knightGroups.size();i++)
        	{
        		KnightGroup knightGroup = knightGroups.get(i);
        		if(!knightGroup.isDead())
        		{
        			if(!knightGroup.isInAction())
        			{
        				while(knightList.size()>0 && !knightGroup.isGroupFull())
        				{
        					IKnight knight = knightList.remove(0);
        	    			knightGroup.addAGroupMember(knight);
        				}
        			}
        			knightGroup.setInAction(true);
        			groupList.add(knightGroup);
        		} 
        	}
        	while(knightList.size()>0)
        	{
        		KnightGroup knightGroup = new KnightGroup(knightSize,startingCastle);
        		while(!knightGroup.isGroupFull() && knightList.size()>0)
        		{
        			IKnight knight = knightList.remove(0);
        			knightGroup.addAGroupMember(knight);
        		}
        		knightGroup.setInAction(true);
        		groupList.add(knightGroup);
        	}
        	 
    	}    	
    	knightGroups = groupList;
    }

	private Collection<KnightGroup> findCastleGroupAndAssign(List<IKnight> knightList) {
		List<KnightGroup> collGroup = new ArrayList<KnightGroup>();
		ICastle[] mineCastles = getCastles();
		ICastle[] hisCastles = World.getOtherCastles();
		ICastle[] allCastles = new ICastle[mineCastles.length+hisCastles.length];
		int i=0;
		for(ICastle castle: mineCastles)
		{
			allCastles[i] = castle;
			i++;
		}
		for(ICastle castle: hisCastles)
		{
			allCastles[i] = castle;
			i++;
		}
		while(knightList.size()>0)
		{
			IKnight knight = knightList.remove(0);
			
			Arrays.sort(allCastles, new ClosestIObject(knight));
			KnightGroup group = castleGroupMap.get(allCastles[0].getId());
			if(group == null)
			{
				group = new KnightGroup(knightSize,allCastles[0]);
				collGroup.add(group);
			}
			group.addAGroupMember(knight);
			if(group.isGroupFull())
			{
				castleGroupMap.remove(allCastles[0].getId());
				knightGroups.add(group);
			}
			else
			{
				castleGroupMap.put(allCastles[0].getId(), group);
			}
		}
		return knightGroups;
	}

	private void setUpMoves() {		
		enemySet.clear();	
		allLandOwned=true;
		totalSize = 0;
		enemyPoints.clear();
		
		//createQuadrants(quadrantSize);
		WayPoint[] quads = getQuadrants();
		for(WayPoint quad: quads)
		{
			quad.setValues();
		}
	}

    public void addEnemy(IObject enemy) {
		if(!enemySet.contains(enemy.getId()))
		{
			enemySet.add(enemy.getId());
			WayPoint wp = new WayPoint(enemy.getX(),enemy.getY(),"Enemy: "+enemy.getId(),PACK_SIZE);
			wp.setFocusObject(enemy);
			enemyPoints.add(wp);
			totalSize++;
		}
		
	}

	public void orderToTopRight(IObject obj)
    {
    	int dir = MOVE_S;
    	Point pos = World.getPositionAfterMove(obj.getX(), obj.getY(), dir);
    	if(pos == null || pos.x == obj.getX() && pos.y == obj.getY())
    	{
    		dir = MOVE_E;    		
    	}
    	if(obj instanceof IKnight)
    	{
    		move((IKnight)obj, dir);
    	}
    	if(obj instanceof IPeasant)
    	{
    		move((IPeasant)obj,dir);
    	}
    }
	
	public void createQuadrants(int quadrantSize)
	{				
		int jump = (quadrantSize*2)+1;
		quadrants = new HashMap<String,WayPoint>();
		int x = 0;
		int y = 0;
		for(int i=0;((i*jump)+quadrantSize)<World.WIDTH;i++)
		{		
			x = quadrantSize+(i*jump);			
			for(int j=0;((j*jump)+quadrantSize)<World.HEIGHT;j++)
			{				
				y = quadrantSize+(j*jump);
				WayPoint ywp = new WayPoint(x,y,"Quadrant_x"+x+"_y"+y,quadrantSize);
				quadrants.put(ywp.getName(), ywp);
			}
			y = y+quadrantSize+1;
			if(y<World.HEIGHT)
			{
				WayPoint ywp = new WayPoint(x,y,"Quadrant_x"+x+"_y"+y,quadrantSize);
				quadrants.put(ywp.getName(), ywp);
			}
		}
		x = x+quadrantSize+1;
		if(x<World.WIDTH)
		{
			WayPoint ywp = new WayPoint(x,y,"Quadrant_x"+x+"_y"+y,quadrantSize);
			quadrants.put(ywp.getName(), ywp);
		}
	}
	
	public Position createDefaultPosisiton(int x, int y)
	{
		return new Position(x,y,"Position_X("+x+")_Y("+y+")");
	}
	
	public WayPoint[] getQuadrants()
	{
		return quadrants.values().toArray(new WayPoint[0]);
	}
	
	public Integer getNumMovesTo(IObject myDude, IObject thatDude)
	{
		return myDude.getDistanceTo(thatDude.getX(), thatDude.getY());
	}
	
	public Position getGridPosition(int x, int y, int dir)
	{
		return getGridPosition(x, y, dir, 1);
	}
	public Position getGridPosition(int x, int y, int dir, int moves)
	{
		int numMoves = moves -1;
		int[] adds = directionMap.get(dir);
		int xMove = adds[0] == 0?0:adds[0]<0?adds[0]+(numMoves*-1):adds[0]+numMoves;
		int yMove = adds[1] == 0?0:adds[1]<0?adds[1]+(numMoves*-1):adds[1]+numMoves;
		int x1 = x+xMove;
		int y1 = y+yMove;
		Position pos = createDefaultPosisiton(x1, y1);
		return pos;
	}
	
	public interface IStrategy
	{
		public void runStrategy(IGroup group);
		public boolean isComplete();
		public String getStrategyName();
		public Position move(IObject subject, Position pos);
	}
	
	public abstract class BaseStrategy
	{
		private MyRuler myRuler;
		Map<Integer, Stack<WayPoint>> stackMap = new HashMap<Integer, Stack<WayPoint>>();
		protected Map<String, IGroup> assignedEnemies = new HashMap<String,IGroup>();

		public BaseStrategy(MyRuler aRuler)
		{
			super();
			this.myRuler=aRuler;
		}
		
		public MyRuler getRuler()
		{
			return myRuler;
		}
		
		public void aroundTheHorn(IObject[] subjects)
		{
			for(IObject subject: subjects)
			{
				aroundTheHorn(subject);
			}
		}
		
		public abstract void runStrategy(IGroup group);
		
		public void aroundTheHorn(IGroup group)
		{
			Stack<WayPoint> stack = stackMap.get(group.getGroupId());
			if(stack == null || stack.size()== 0)
			{
				stack = createStack(group);
				stackMap.put(group.getGroupId(), stack);
			}
			WayPoint curPos = stack.peek();
			IObject[] members = group.getGroupMembers();
			boolean allInPos = true;
			for(IObject member: members)
			{
				if(!curPos.inTheGrid(member.getX(), member.getY()))
				{
					allInPos = false;
				}
			}
			if(allInPos)
			{
				stack.pop();
				if(stack.isEmpty())
				{
					stack = createStack(group);
					stackMap.put(group.getGroupId(), stack);
				}
				curPos = stack.peek();
			}
			for(IObject member: members)
			{
				move(member,curPos);
			}
		}
		
		public Stack<WayPoint> createStack(IGroup group)
		{
			Stack<WayPoint>stack = new Stack<WayPoint>();
			for(int i=0;i<8;i++)
			{
				stack.push(positions.get(i));
			}	
			return stack;
		}
		public void aroundTheHorn(IObject subject)
		{
			boolean inPos = false;
			int i=0;
			WayPoint[] allPos = positions.values().toArray(new WayPoint[0]);
			while(i<allPos.length && !inPos)
			{
				if(allPos[i].inTheGrid(subject.getX(), subject.getY()))
				{
					inPos = true;
				}
				else
				{
					i++;
				}
			}
			if(inPos)
			{
				int newPos = i==(allPos.length-1)?0:i+1;
				move(subject,allPos[newPos]);
			}
			else
			{
				move(subject,allPos[0]);
			}
			
		}
		
		
		public void goToTheMiddle(IObject[] subjects)
		{
			for(IObject subject: subjects)
			{
				goToTheMiddle(subject);
			}
		}
		public void goToTheMiddle(IObject subject)
		{
			IRuler ruler = World.getLandOwner(subject.getX(), subject.getY());
			boolean badPos =false;
			if(ruler != null && ruler.equals(getMyRuler()))
			{
				badPos=true;
			}
			if(middleOfMap.inTheGrid(subject.getX(), subject.getY()) && badPos)
			{
				rulerMove(subject, 1);
			}			
			else if(!middleOfMap.inTheGrid(subject.getX(), subject.getY()) || badPos)
			{
				move(subject, middleOfMap);
			}
		}
		
		public Position move(IObject subject, Position pos)
		{
			pos.setDirection(subject.getDirectionTo(pos.getX(), pos.getY()));
			move(subject, pos.getDirection(), pos);
			rulerMove(subject, pos.getDirection());
			return pos;
		}
		private void rulerMove(IObject subject, int dir)
	    {
			if (subject.isAlive()) {
				//trackMap.get(subject.getId()).addMove();
				Point pnt = World.getPositionAfterMove(subject.getX(), subject
						.getY(), dir);
				if (pnt != null) {
					if (subject instanceof IPeasant) {
						getRuler().move((IPeasant) subject, dir);
					} else {
						knightMove(subject, dir);
					}
				}
			}
	    }
		private void knightMove(IObject subject, int dir) 
		{
			Point origPnt = World.getPositionAfterMove(subject.getX(), subject.getY(), dir);
			List<IKnight> enemyKnights = new ArrayList<IKnight>();
			for(int i=0;i<8;i++)
			{
				Point pnt = World.getPositionAfterMove(subject.getX(), subject.getY(), i+1);
				if(pnt != null)
				{
					IObject enemy = World.getObjectAt(pnt.x, pnt.y);
					if(enemy != null && enemy instanceof IKnight && !enemy.getRuler().equals(getMyRuler()))
					{
						enemyKnights.add((IKnight)enemy);
					}
				}
			}
			IKnight cap = null;
			IKnight[] knights = enemyKnights.toArray(new IKnight[0]);
			for(IKnight knight: knights)
			{
				if(cap == null)
				{
					cap = knight;
				}
				else
				{
					if(knight.getStrength()<cap.getStrength())
					{
						cap = knight;
					}
				}
			}
			if(cap == null)
			{
				IObject origCap = World.getObjectAt(origPnt.x, origPnt.y);
				if (origCap == null) {
					getRuler().move((IKnight) subject, dir);
				} else {
					getRuler().capture((IKnight) subject, dir);
				}
			}
			else
			{
				int newDir = subject.getDirectionTo(cap.getX(), cap.getY());
				getRuler().capture((IKnight)subject, newDir);
			}
			
			
		}

		public void move(IObject subject, Integer dir, Position position)
	    {
	    	move(subject, dir, position, 0);
	    }
	    
	    public void move(IObject subject, Integer dir, Position position, int times)
	    {
	    	move(subject, dir, position, times, new HashSet<Integer>());
	    }
	    
	    public void move(IObject subject, Integer dir, Position position, int times, Set<Integer> movesMade)
	    {
	    	movesMade.add(dir);
	    	if(times>8)
	    	{
	    		// looks like there is no place to go.. So go the direction originally decided
	    		rulerMove(subject, position.getDirection());
	    		return;
	    	}
	    	Point ps = World.getPositionAfterMove(subject.getX(), subject.getY(), dir);
	    	if(ps == null)
	    	{
	    		move(subject, getNewDirection(subject, movesMade, position),position, times+1, movesMade);
	    	}
	    	else
	    	{
	    		if(canNotMove(ps,subject))
	    		{
	    			move(subject, getNewDirection(subject, movesMade, position),position, times+1, movesMade);
	    		}
	    		else
	    		{
	    			position.setDirection(dir);
	    		}
	    	}
	    }
	    
	    private boolean canNotMove(Point ps, IObject subject) {
			boolean cannotMove = false;
			if(ps.x == subject.getX() && ps.y == subject.getY())
			{
				cannotMove = true;
			}
			else
			{
				IObject obj = World.getObjectAt(ps.x, ps.y);
				if(obj != null)
				{
					if(subject instanceof IPeasant)
					{
						cannotMove = true;
					}
					else if(obj.getRuler().equals(getRuler()))
					{
						cannotMove = true;
					}
				}				
			}
			return cannotMove;
		}

		private int getNewDirection(IObject obj, Set<Integer> movesMade, Position position) {
	    	int distance = 9999;
	    	int direction = 0;
			for(int i=0;i<8;i++)
			{
				int myDir = i+1;
				if(!movesMade.contains(myDir))
				{
					Point pnt = World.getPositionAfterMove(obj.getX(), obj.getY(), myDir);
					if(pnt != null)
					{
						int dis = position.distanceTo(pnt.x, pnt.y);
						if(dis<distance)
						{
							distance = dis;
							direction = myDir;
						}
					}
				}
			}
			return direction;
		}
		
		protected void runAway(IObject[] subjects,IObject enemyMine) {
			for(IObject subject: subjects)
			{
				int badDir = subject.getDirectionTo(enemyMine.getX(), enemyMine.getY());
				Set<Integer> badDirs = new HashSet<Integer>();
				badDirs.add(badDir==8?1:badDir+1);
				badDirs.add(badDir);
				badDirs.add(badDir==1?8:badDir-1);
				
			    int newDir = badDir + 4;
			    newDir = newDir>8?newDir-8:newDir;
			    boolean directionFnd = false;
			    while(!directionFnd && badDirs.size()!=8)
			    {
			    	Position runPos = getGridPosition(subject.getX(), subject.getY(), newDir, 4);
			    	if(runPos.valid())
			    	{
			    		directionFnd = true;
			    	}
			    	else
			    	{
			    		badDirs.add(newDir);
			    		newDir = newDir==8?1:newDir+1;
			    		while(badDirs.contains(newDir) && badDirs.size() != 8)
			    		{
			    			newDir = newDir==8?1:newDir+1;
			    		}
			    	}
			    }
			    if(!directionFnd)
			    {
			    	newDir = badDir + 4;
				    newDir = newDir>8?newDir-8:newDir;
			    }
			    Position runPos = getGridPosition(subject.getX(), subject.getY(), newDir);
			    move(subject, subject.getDirectionTo(runPos.x, runPos.y),runPos,0,badDirs);
				rulerMove(subject, runPos.getDirection());				
			}
		}
	}
	public class PeasantStrategy extends BaseStrategy implements IStrategy
	{

		
		public PeasantStrategy(MyRuler ruler) {
			super(ruler);
		}
		private WayPoint assignedQuadrant;
		private Map<String,IObject>assignedGridPositions = new HashMap<String,IObject>();
		@Override
		public void runStrategy(IGroup group) {
			WayPoint[] quads = getRuler().getQuadrants();
			IKnight[] enemyArray = World.getOtherKnights();
			Arrays.sort(enemyArray, new ClosestIObject(group.getGroupLeader()));
			IObject[] peasants = group.getGroupMembers();
			WayPoint curPosition = new WayPoint(group.getGroupLeader().getX(),group.getGroupLeader().getY(),Integer.toString(group.getGroupId()),DANGER_ZONE);
			
			boolean runAway = false;
			if(enemyArray != null && enemyArray.length>0)
			{
				if(curPosition.inTheGrid(enemyArray[0].getX(), enemyArray[0].getY()))
				{
					runAway = true;
				}
			}
			if(runAway)
			{
				if(assignedQuadrant != null)
				{
					assignedQuadrant.setAssigned(false);
				}
				assignedQuadrant = null;
				runAway(peasants,enemyArray[0]);				
			}
			else
			{
				if(assignedQuadrant == null || assignedQuadrant.isGridOwned())
				{
					Arrays.sort(quads, new ClosestGridPosition(group.getGroupLeader()));
					boolean quadFnd = false;
					for(int i=0;i<quads.length && !quadFnd;i++)
					{
						if(!quads[i].isAssigned())
						{
							assignedQuadrant = quads[i];
							quadFnd = true;
							quads[i].setAssigned(true);
						}
					}
				}
				if (assignedQuadrant != null && !assignedQuadrant.isGridOwned()) {
					takeLand(peasants);
				}
				else
				{
					if(quads[0].isGridOwned())
					{
						
						if(allLandOwned)
						{
							if(!assimilation)
							{
								System.out.println("You have been assimilated, It took me "+World.getCurrentTurn()+" moves to humiliate you");
							}							
							assimilation=true;							
						}
						else
						{
							for(WayPoint quad: quads)
							{
								if(!quad.isGridOwned())
								{
									assignedQuadrant = quad;
									break;
								}
							}
							if(assignedQuadrant != null)
							{
								takeLand(peasants);
							}							
						}												
					}
					else
					{
						assignedQuadrant=quads[0];
						takeLand(peasants);
					}
				}
			}
		}

		private void takeLand(IObject[] peasants) {
			for (IObject obj : peasants) {
				IPeasant peasant = (IPeasant) obj;
				try {

					Position pos = assignedQuadrant.getClosestGridPosition(peasant, true);
					if (pos != null) {
						IObject subject = assignedGridPositions.get(pos
								.getName());
						if (subject == null || subject.equals(peasant)) {
							int dir = peasant.getDirectionTo(
									pos.getX(), pos.getY());
							pos.setDirection(dir);
							move(peasant, pos);
							assignedGridPositions.put(pos.getName(),
									peasant);
						}
					}
				} catch (RuntimeException e) {
					System.out.println("Got exception moving peasant: "
							+ peasant.getId());
					e.printStackTrace();
				}
			}
			
		}		

		@Override
		public boolean isComplete() {
			// Never complete
			return false;
		}

		@Override
		public String getStrategyName() {
			return "Peasant_Strategy";
		}
		
	}
	
	public class ProtectPosition extends BaseStrategy implements IStrategy
	{

		private int defaultGridSize = 3;
		public ProtectPosition(IObject center) {
			super(getMyRuler());
			this.protectObject=center;
			if(protectObject != null)
			{
				protectPosition = new WayPoint(protectObject.getX(), protectObject.getY(),"ProtectThisGrid",defaultGridSize);
			}
		}
		
		public ProtectPosition(int x, int y)
		{
			super(getMyRuler());
			protectPosition = new WayPoint(x,y,"ProtectPosition:x("+x+") y("+y+")",defaultGridSize);
		}
		
		public ProtectPosition(IObject center, int gridSize) {
			super(getMyRuler());
			this.protectObject=center;
			this.defaultGridSize=gridSize;
			if(protectObject != null)
			{
				protectPosition = new WayPoint(protectObject.getX(), protectObject.getY(),"ProtectThisGrid",defaultGridSize);
			}
		}
		
		public ProtectPosition(int x, int y, int gridSize)
		{
			super(getMyRuler());
			this.defaultGridSize=gridSize;
			protectPosition = new WayPoint(x,y,"ProtectPosition:x("+x+") y("+y+")",defaultGridSize);
		}

		private IObject protectObject;
		private WayPoint protectPosition = null;
		
		@Override
		public String getStrategyName() {
			return "Protect_The_Grid";
		}

		@Override
		public boolean isComplete() {
			return !protectObject.isAlive();
		}

		@Override
		public void runStrategy(IGroup group) 
		{
			IObject[] members = group.getGroupMembers();
			IObject[] enemies = World.getOtherKnights();
			Arrays.sort(enemies, new ClosestEnemyToPosition(protectPosition));
			if (protectObject != null && !protectObject.getRuler().equals(getRuler())) 
			{
				for (IObject member : members) {
					IKnight curKnight = (IKnight) member;
					moveAndCapture(curKnight, protectObject);
				}
			}
			else
			{		
				
				for (IObject member : members) 
				{
					IKnight curKnight = (IKnight) member;
					if(enemies.length>0 && protectPosition.inTheGrid(enemies[0].getX(), enemies[0].getY()))
					{
						moveAndCapture(curKnight, enemies[0]);
					}
					
				}
			}
		}
		
		public void moveAndCapture(IKnight knight, IObject enemy) {
	    	if (enemy != null) {
				// find the next position in the direction of the enemy
				Integer dir = knight.getDirectionTo(enemy.getX(), enemy.getY());
				Position pos = new Position(enemy.getX(), enemy.getY(), enemy
						.getClass().getName()
						+ enemy.getId());
				pos.setDirection(dir);
				move(knight, pos);
			}
	    }
		
	}
	public class CherryPick extends BaseStrategy implements IStrategy
	{
		private ICastle castleProtect = null;
		private Position castlePosition = null;
		private WayPoint lieInWait = null;
		public CherryPick(MyRuler ruler, ICastle castle) {
			super(ruler);
			this.castleProtect=castle;
			castlePosition = new Position(castle.getX(),castle.getY(),"Castle: "+castle.getId());
			WayPoint[] allPos = positions.values().toArray(new WayPoint[0]);
			Arrays.sort(allPos, new ClosestPosition(castle));
			lieInWait=allPos[0];			
		}
		
		public int getGroupPoints(IGroup group)
		{
			IObject[] members = group.getGroupMembers();
			int pnts=0;
			for(IObject member: members)
			{
				if(member.isAlive() && member instanceof IKnight)
				{
					IKnight knight = (IKnight)member;
					pnts += knight.getStrength();
				}
			}
			return pnts;
		}
		
		/**
		 * New way
		 */
		@Override
		public void runStrategy(IGroup group) 
		{
			lieInWait = new WayPoint(lieInWait.getX(),lieInWait.getY(),"Lie_In_Wait: "+castleProtect.getId(),DANGER_ZONE,true);
			IObject[] targets = lieInWait.getEnemies();
			IObject[] members = group.getGroupMembers();
			boolean run = false;
			IObject enemyMine = null;
			IKnight[] enemies = World.getOtherKnights();
			int pnts = getGroupPoints(group);
			for(IObject member: members)
			{
				WayPoint curPosition = new WayPoint(member.getX(),member.getY(),"currentPosition",DANGER_ZONE);
				Arrays.sort(enemies, new ClosestIObject(member));
				if(!castleProtect.getRuler().equals(getMyRuler()) && castlePosition.getEnemyPoints()<pnts)
				{
					moveAndCapture((IKnight)member, castlePosition);
				}
				else
				{
					if(enemies != null && enemies.length>0)
					{												
						if(curPosition.getEnemyPoints()>pnts)
						{
							run = true;							
							enemyMine=enemies[0];
						}
					}
					if(run)
					{
						runAway(new IObject[]{member}, enemyMine);
					}
					else
					{
						if(targets!=null && targets.length>0)
						{
							moveAndCapture((IKnight)member, new Position(targets[0].getX(),targets[0].getY(),Integer.toString(targets[0].getId())));
						}
						else
						{
							if(!lieInWait.inTheGrid(member.getX(), member.getY()))
							{
								move(member, lieInWait);
							}
							else
							{
								comeTogether_RightNow(member, group);
							}
						}						
					}
					
				}
			}
			
		}
		
		private void comeTogether_RightNow(IObject member, IGroup group) 
		{
			if(member.getId() == group.getGroupLeader().getId())
			{
				if(!(member.getX() == lieInWait.getX() && member.getY() == lieInWait.getY()))
				{
					move(member,lieInWait);
				}
			}
			else
			{
				int dir = member.getDirectionTo(lieInWait.getX(), lieInWait.getY());
				Position pos = getGridPosition(lieInWait.getX(), lieInWait.getY(), dir);
				// Wouldn't be in this method if this position is an enemy of some sort
				if(!pos.isKnight())
				{
					move(member,lieInWait);
				}
			}
		}

		public void moveAndCapture(IKnight knight, Position enemy) 
		{
	    	if (enemy != null) {
				// find the next position in the direction of the enemy
				Integer dir = knight.getDirectionTo(enemy.getX(), enemy.getY());
				enemy.setDirection(dir);
				move(knight, enemy);
			}
	    }

		@Override
		public String getStrategyName() {
			return "Cherry_Picker";
		}

		@Override
		public boolean isComplete() {
			return false;
		}
		
	}
	public class TopCorner extends BaseStrategy implements IStrategy
	{

		public TopCorner(MyRuler ruler) {
			super(ruler);
		}

		@Override
		public String getStrategyName() {
			return "TOP_CORNER";
		}

		@Override
		public boolean isComplete() {
			return false;
		}

		@Override
		public void runStrategy(IGroup group) {
			IObject[] peasants = group.getGroupMembers();
			for(IObject obj: peasants)
			{
				getRuler().orderToTopRight(obj);
			}
		}
		
	}
	
	public class TheWeakestLink extends BaseStrategy implements IStrategy
	{
		private boolean noEnemies = false;
		public TheWeakestLink(MyRuler ruler) {
			super(ruler);
		}
		
		@Override
		public void runStrategy(IGroup group) {

			if (!noEnemies) 
			{
				goAfterEnemies(group);
			}
			else
			{
				aroundTheHorn(group);							
			}
		}
		
		private void goAfterEnemies(IGroup group) {
			KnightGroup knightGroup = (KnightGroup) group;
			IObject[] knights = knightGroup.getGroupMembers();
			WayPoint[] enemyArray = getEnemyArray();
			for (IObject obj : knights) {
				IKnight curKnight = (IKnight) obj;
				IKnight enemyMine = fleeEnemy(curKnight);
				if (enemyMine != null) {
					runAway(new IObject[] { curKnight }, enemyMine);
				} else {
					Position target = isEnemyCastleNear(curKnight);
					if (target == null) {
						boolean moreEnemies = enemyArray.length > 0;
						if (moreEnemies) {
							Arrays.sort(enemyArray,
									new ClosestGridPosition(group
											.getGroupLeader()));
							moveAndCapture(curKnight, enemyArray[0]);
						} else {
							noEnemies = true;
							if (!assimilation) {
								aroundTheHorn(curKnight);

							}
						}
					} else {
						moveAndCapture(curKnight, target);
					}
				}
			}			
		}

		private Position isEnemyCastleNear(IKnight knight) {
			ICastle[] allCastles = World.getOtherCastles();
			if(allCastles == null || allCastles.length == 0)
			{
				return null;
			}
			WayPoint curPos = new WayPoint(knight.getX(), knight.getY(), "nearCastle");
			for(ICastle castle:allCastles)
			{
				if(curPos.inTheGrid(castle.getX(), castle.getY()))
				{
					return new Position(castle.getX(),castle.getY(),"Castle: "+castle.getId());
				}
			}
			return null;
		}
		
		public IKnight fleeEnemy(IKnight thisKnight)
		{
			IKnight fleeFromKnight = null;
			IObject[] enemies = World.getOtherKnights();
			Arrays.sort(enemies, new ClosestIObject(thisKnight));
			WayPoint curPos = new WayPoint(thisKnight.getX(), thisKnight.getY(), "current Postion");
			if(enemies != null && enemies.length> 0 && curPos.inTheGrid(enemies[0].getX(), enemies[0].getY()))
			{
				IKnight enemyMine = (IKnight)enemies[0];
				if(enemyMine.getStrength()>thisKnight.getStrength() && thisKnight.getStrength()<31)
				{
					fleeFromKnight = enemyMine;
				}
			}
			return fleeFromKnight;
		}

		private WayPoint[] getEnemyArray() {
			return enemyPoints.toArray(new WayPoint[0]);
		}
		
		public void moveAndCapture(IKnight knight, Position enemy) 
		{
	    	if (enemy != null) {
				// find the next position in the direction of the enemy
				Integer dir = knight.getDirectionTo(enemy.getX(), enemy.getY());
				enemy.setDirection(dir);
				move(knight, enemy);
			}
	    }
		
		@Override
		public boolean isComplete() {
			// never complete
			return false;
		}
		@Override
		public String getStrategyName() {
			return "First_Knight_Strategy";
		}
		
	}
	
	public class Position
	{
		
		protected static final int NOT_OWNED_LAND = -3;
		protected static final int OTHER_OWNED_LAND = -4;
		protected static final int DEF_CASTLE_VALUE = -150;
		protected static final int DISTANCE_VALUE = 13;
		
		int x;
		int y;
		int direction;
		String name;
		private boolean castle = false;
		private boolean peasant = false;
		private boolean knight = false;
		private boolean owned = false;
		private IObject object;
		private IRuler landOwner;
		private int landPoint = 0;
		private int enemyPoint = 0;
		protected int castlePoint = 0;
		
		public Position(int x, int y, String name)
		{
			super();
			this.x=x;
			this.y=y;
			this.name=name;
			init();
		}
		
		public void init() {
			landPoint = 0;
			enemyPoint = 0;
			castlePoint = 0;
			owned = false;
			if(valid())
			{
				IObject obj = World.getObjectAt(x, y);
				if(obj != null)
				{
					object = obj;
					if(obj instanceof ICastle)
					{
						castle =true;
						owned = true;
					}
					if(!obj.getRuler().equals(getMyRuler()))
					{
						getMyRuler().addEnemy(obj);
						if(castle)
						{
							if(getMyRuler().getCastleValue() != null)
							{
								castlePoint = getMyRuler().getCastleValue();
							}
							else
							{
								castlePoint = DEF_CASTLE_VALUE;
							}
						}
						if(obj instanceof IKnight)
						{
							knight = true;
							enemyPoint = ((IKnight)obj).getStrength();
						}
						else
						{
							peasant = true;
						}
					}
						
				}
				landOwner = null;
				if(!castle)
				{
					landOwner = World.getLandOwner(x, y);
					if(landOwner == null)
					{
						landPoint = NOT_OWNED_LAND;
					}
					else
					{
						if(landOwner.equals(getMyRuler()))
						{
							owned = true;
						}
						else
						{
							landPoint = OTHER_OWNED_LAND;
						}
					}
				}
				
			}
		}

		public Position(Point pnt, String name)
		{
			this(pnt.x,pnt.y,name);
		}
		public int getX() {
			return x;
		}
		public int getY() {
			return y;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int distanceTo(int argX, int argY)
		{
			Point point = new Point(x,y);
			Double myDouble = point.distance(argX,argY);
			return myDouble.intValue();
		}
		
		public int getDirection()
		{
			return direction;
		}
		
		public void setDirection(int dir)
		{
			this.direction=dir;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final Position other = (Position) obj;
			if (x != other.x)
				return false;
			if (y != other.y)
				return false;
			return true;
		}

		public boolean valid()
		{
			if(x <0 || y <0 ||
			   x >= World.WIDTH || y >= World.HEIGHT)
			   {
				return false;
			   }
			return true;
		}
		
		public boolean isCastle(){return castle;}		
		public boolean isPeasant(){return peasant;}
		public boolean isKnight(){return knight;}
		public boolean landOwned(){return owned;}
		
		public int getCastlePoints(){return castlePoint;}
		public int getEnemyPoints(){return enemyPoint;}
		public int getLandPoints(){return landPoint;}
		
		public IObject getObjectAt(){return object;}

		public String toString()
		{
			return "Point = ("+x+","+y+")";
		}
	}
	
	public class WayPoint extends Position
	{								
		private Set<Position> grid;
		private int gridSize = 2;		
		private boolean assigned = false;
		private boolean gridOwned = false;
		private int enemyValue;
		private int landValue;
		private int tempValue;
		private List<IObject> enemies = null;
		private boolean track = false;
		private IObject centerObject;

		public WayPoint(int x, int y, String name, int gridSize, boolean trackEnemies) {
			super(x, y, name);
			this.setTrackEnemies(trackEnemies);			
			this.gridSize=gridSize;
			createGrid();			
		}
		
		public WayPoint(int x, int y, String name) {
			super(x, y, name);
			createGrid();
		}
		
		public WayPoint(int x, int y, String name, int gridSize) {
			super(x, y, name);
			this.gridSize=gridSize;
			createGrid();
		}
		
		public WayPoint(Position pos)
		{
			this(pos.getX(),pos.getY(), pos.getName());
		}

		public void setTrackEnemies(boolean argTrack)
		{
			this.track=argTrack;
			if(track)
			{
				enemies = new ArrayList<IObject>();
			}
		}
		
		public IObject[] getEnemies()
		{
			if(!track)
			{
				return null;
			}
			return enemies.toArray(new IObject[0]);
		}
		
		public void createGrid() 
		{
			landValue=0;
			enemyValue=0;
			grid = new HashSet<Position>();
			if(valid())
			{
				addPosition(this);
			}			
			gridLoop(gridSize);
		}

		public void addPosition(Position addPos)
		{
			if(!addPos.landOwned())
			{
				allLandOwned = false;
			}
			castlePoint += addPos.getCastlePoints();
			landValue += addPos.getLandPoints();
			enemyValue += addPos.getEnemyPoints();
			grid.add(addPos);
			if(track)
			{
				if(addPos.isKnight() && !addPos.getObjectAt().getRuler().equals(getMyRuler()))
				{
					enemies.add(addPos.getObjectAt());
				}
				if(addPos.isPeasant() && !addPos.getObjectAt().getRuler().equals(getMyRuler()))
				{
					enemies.add(addPos.getObjectAt());
				}
			}
		}
		
		public int getGridSize() {
			return gridSize;
		}

		public void setGridSize(int gridSize) {
			this.gridSize = gridSize;
		}

		public IObject getIObject()
		{
			return centerObject;
		}
		
		public void setFocusObject(IObject obj)
		{
			this.centerObject=obj;
		}
		
		public boolean isGridOwned(){return gridOwned;}
		
		/**
		 * Pay attention.. this is the key to my entire strategy
		 * <BR>
		 *  I give you "The Grid"
		 * @param gridSize
		 */
		private void gridLoop(int gridSize) {
			for(int i=0;i<8;i++)
			{
				Position newPos = getGridPosition(getX(), getY(), i+1, gridSize);
				int a = i+1;
				if(a%2==0)
				{
					a = a==8?1:a+1;
				}
				int direction = a+2>8?1:a+2;
				for (int j=0;j<gridSize-1;j++)
				{
					Position rightPos = getGridPosition(newPos.getX(), newPos.getY(), direction, j+1);
					if(rightPos.valid())
					{
						addPosition(rightPos);
					}
				}
				if(newPos.valid())
				{
					addPosition(newPos);
				}
			}
			// love recursion
			if(gridSize>1)
			{
				gridLoop(gridSize-1);
			}
		}
		public int getWayPointValue(IObject obj)
		{
			int distance = obj.getDistanceTo(getX(), getY())*DISTANCE_VALUE;
			int sum = 0;
			if(obj instanceof IKnight)
			{
				sum = getEnemyValue() + castlePoint;								
			}
			else
			{
				sum = getEnemyValue()+getLandValue();
			}
			tempValue = sum+distance;
			return tempValue;
		}
		/**
		 * If the entire grid is owned by another owner this value will
		 * be  {@link #getTheGrid()}.length * {@value #OTHER_OWNED_LAND}
		 * if it's entirely unowned this value will be 
		 * {@link #getTheGrid()}.length * {@value #NOT_OWNED_LAND}
		 * 
		 * Or some combination thereof
		 * 
		 * If the grid is entirely owned.. the value returned is zero.
		 * @return
		 */
		public int getLandValue()
		{
			return landValue;
		}
		public int getEnemyValue()
		{
			return enemyValue;
		}

		public void setValues()
		{
			landValue=0;
			enemyValue=0;
			gridOwned = true;
			Position[] posArray = getTheGrid();
			for(Position pos:posArray)
			{
				pos.init();
				if(!pos.landOwned())
				{
					allLandOwned = false;
					gridOwned = false;
				}
				castlePoint = pos.getCastlePoints();
				landValue += pos.getLandPoints();
				enemyValue += pos.getEnemyPoints();
			}
			if(landValue == 0)
			{
				landValue = 5000;
			}
		}
		public void setValues(IObject[] enemies)
		{
			Position[] posArray = getTheGrid();

			landValue = 0;
			for(Position pos: posArray)
			{
				IRuler owner = World.getLandOwner(pos.x,
						pos.y);
				if(owner == null)
				{
					if(!pos.isCastle())							
					{
						landValue += NOT_OWNED_LAND;
						allLandOwned=false;
					}

				}
				else
				{
					if(!owner.equals(getMyRuler()))
					{
						landValue += OTHER_OWNED_LAND;
						allLandOwned=false;
					}
				}
			}

			enemyValue=0;
			for(IObject obj: enemies)
			{
				if(obj instanceof IKnight && inTheGrid(obj.getX(), obj.getY()))
				{
					enemyValue += ((IKnight)obj).getStrength();
				}
			}
		}

		
		
		public boolean isAssigned() {
			return assigned;
		}

		public void setAssigned(boolean assigned) {
			this.assigned = assigned;
		}

		public Position getClosestGridPosition(IObject argObject, boolean unowned)
		{
			Position[] posArray = getTheGrid();
			Arrays.sort(posArray, new ClosestPosition(argObject));
			
			if(!unowned)
			{
				return posArray[0];
			}
			boolean fnd = false;
			Position pos = null;
			for(int i=0;i<posArray.length&&!fnd;i++)
			{
				IRuler owner = World.getLandOwner(posArray[i].x, posArray[i].y);
				if((owner == null && !posArray[i].isCastle()) || (owner != null && !owner.equals(getMyRuler())))
				{					
					fnd =true;
					pos = posArray[i];
				}
			}
			return pos;
		}
		
		public Position[] getTheGrid()	
		{
			Position[] posArray = grid.toArray(new Position[0]);
			return posArray;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((grid == null) ? 0 : grid.hashCode());
			result = prime * result + gridSize;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			final WayPoint other = (WayPoint) obj;
			if (grid == null) {
				if (other.grid != null)
					return false;
			} else if (!grid.equals(other.grid))
				return false;
			if (gridSize != other.gridSize)
				return false;
			return true;
		}

		public boolean inTheGrid(int x, int y)
		{
			Position pos = new Position(x,y,"Position_X("+x+")_Y("+y+")");
			return grid.contains(pos);
		}
		
		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			String LB = "\n";
			sb.append("Number of Positions in Grid ("+this.getName()+")= "+grid.size());
			sb.append(LB);
			sb.append("Now Printing Grid");
			sb.append(LB);
			sb.append(super.toString());
			sb.append(LB);
			Position[] positions = grid.toArray(new Position[0]);
			for(Position position: positions)
			{
				if(!(position.x == this.x && position.y == this.y))
				{
					sb.append(position.toString());
					sb.append(LB);
				}
			}
			return sb.toString();
		}
		
	}
	
	public class ClosestPosition implements Comparator<Position>
	{
		private IObject myObject;
		public ClosestPosition(IObject inObject)
		{
			super();
			this.myObject=inObject;
		}
		
		@Override
		public int compare(Position o1, Position o2) {
			int dist1 = myObject.getDistanceTo(o1.x, o1.y);
			int dist2 = myObject.getDistanceTo(o2.x, o2.y);
			if(dist1<dist2)
			{
				return -1;
			}
			if(dist1>dist2)
			{
				return 1;
			}
			return 0;
		}
		
	}
	
	public class ClosestGridPosition implements Comparator<WayPoint>
	{
		private IObject myObject;
		public ClosestGridPosition(IObject inObject)
		{
			super();
			this.myObject=inObject;
		}
		
		@Override
		public int compare(WayPoint o1, WayPoint o2) {
			int value1 = o1.getWayPointValue(myObject);
			int value2 = o2.getWayPointValue(myObject);
			if(value1 < value2)
			{
				return -1;
			}
			if(value1 > value2)
			{
				return 1;
			}
			return 0;
		}
		
	}
	
	public class ClosestIObject implements Comparator<IObject>
	{
		private IObject myObject;
		public ClosestIObject(IObject inObject)
		{
			super();
			this.myObject=inObject;
		}
		
		@Override
		public int compare(IObject o1, IObject o2) {
			int dist1 = myObject.getDistanceTo(o1.getX(), o1.getY());
			int dist2 = myObject.getDistanceTo(o2.getX(), o2.getY());
			if(dist1<dist2)
			{
				return -1;
			}
			if(dist1>dist2)
			{
				return 1;
			}
			return 0;
		}
		
	}
	
	public class ClosestEnemyToPosition implements Comparator<IObject>
	{
		private Position myObject;
		public ClosestEnemyToPosition(Position inObject)
		{
			super();
			this.myObject=inObject;
		}
		
		@Override
		public int compare(IObject o1, IObject o2) {
			int dist1 = myObject.distanceTo(o1.getX(), o1.getY());
			int dist2 = myObject.distanceTo(o2.getX(), o2.getY());
			if(dist1<dist2)
			{
				return -1;
			}
			if(dist1>dist2)
			{
				return 1;
			}
			return 0;
		}
		
	}
	
	public interface IGroup
	{
		public int getGroupId();
		public IObject getGroupType();
		public int getNumberInGroup();
		public boolean isGroupFull();
		public void setGroupSize(int size);
		public IObject getGroupLeader();
		public IStrategy getStrategy();
		public void setStrategy(IStrategy strategy);
		public void loadGroup(IObject[] members);
		public boolean addAGroupMember(IObject member);
		public IObject[] getGroupMembers();
		public int getAvgGroupStrength();
		public boolean isDead();
		public void runStrategy();
	}
	
	abstract class BaseGroup implements IGroup
	{
		private IObject leader;
		private int groupSize;
		private int id;
		private IStrategy strategy;
		private boolean inAction = false;
		private Map<Integer, IObject> groupMap = new HashMap<Integer,IObject>();
		
		public BaseGroup(int argGroupSize)
		{
			super();
			groupId++;
			id = groupId;
			this.groupSize=argGroupSize;
		}
		
		public void loadGroup(IObject[] members)
		{
			for(IObject member: members)
			{
				addAGroupMember(member);
			}
		}
		
		public boolean addAGroupMember(IObject member)
		{
			boolean memberAdded = false;
			if (member.isAlive()) 
			{
				if(!assignedSubject.contains(member.getId()))
				{
					if (groupMap.size() == 0) {
						leader = member;
					}
					groupMap.put(member.getId(), member);
					assignedSubject.add(member.getId());
					memberAdded = true;
				}
				else
				{
					System.out.println("subject already assigned");
				}
			}
			else
			{
				assignedSubject.remove(member.getId());
			}
			return memberAdded;
		}

		public boolean isInAction() {
			return inAction;
		}

		public void setInAction(boolean action)
		{
			inAction=action;
		}
		@Override
		public int getAvgGroupStrength() {
			if(leader instanceof IPeasant)
			{
				return 100;
			}
			int avg =0;
			int sum =0;
			IObject[] members = groupMap.values().toArray(new IObject[0]);
			for(IObject member: members)
			{
				IKnight knight = (IKnight)member;
				sum += knight.getStrength();
			}
			avg = sum/members.length;
			return avg;
		}

		@Override
		public int getGroupId() {
			return id;
		}

		@Override
		public IObject getGroupLeader() {
			return leader;
		}

		@Override
		public IObject[] getGroupMembers() {
			return groupMap.values().toArray(new IObject[0]);
		}

		@Override
		public int getNumberInGroup() {
			return groupMap.values().size();
		}

		@Override
		public boolean isGroupFull() 
		{
			boolean isFull = groupMap.values().size() >= groupSize;
			if(isFull)inAction = true;
			return isFull;
		}

		@Override
		public void setGroupSize(int size) {
			this.groupSize=size;
		}

		@Override
		public boolean isDead() {
			this.bringOutTheDead();
			return groupMap.values().size() == 0;
		}
		
		private void bringOutTheDead() {
			if(!groupMap.isEmpty())
			{
				IObject[] members = groupMap.values().toArray(new IObject[0]);
				groupMap.clear();
				for(IObject member: members)
				{
					assignedSubject.remove(member.getId());
					addAGroupMember(member);					
				}
				
			}						
		}

		protected void performBaseStrategy() {
			if(this instanceof KnightGroup)
			{
				ProtectPosition strat = new ProtectPosition(getGroupLeader(),7);
				strat.runStrategy(this);
			}
			else
			{
				performBasePeasantStrategy();
			}
			
		}

		private void performBasePeasantStrategy() {
			BaseStrategy strat = new BaseStrategy(getMyRuler())
			{

				@Override
				public void runStrategy(IGroup group) 
				{
					if(group.getGroupMembers().length>0)
					{
						IKnight[] enemyArray = World.getOtherKnights();
						Arrays.sort(enemyArray, new ClosestIObject(group.getGroupLeader()));
						IObject[] peasants = group.getGroupMembers();
						WayPoint curPosition = new WayPoint(group.getGroupLeader().getX(),group.getGroupLeader().getY(),Integer.toString(group.getGroupId()),4);
						
						boolean runAway = false;
						if(enemyArray != null && enemyArray.length>0)
						{
							if(curPosition.inTheGrid(enemyArray[0].getX(), enemyArray[0].getY()))
							{
								runAway = true;
							}
						}
						if(runAway)
						{
							runAway(peasants,enemyArray[0]);				
						}
						
					}
				}
	
			};
			
			strat.runStrategy(this);
			
		}

		@Override
		public IObject getGroupType() {
			return leader;
		}

		@Override
		public IStrategy getStrategy() {
			return strategy;
		}

		@Override
		public void runStrategy() {
			if(strategy != null)
			{
				if(inAction || allIn)
				{
					strategy.runStrategy(this);
				}	
				else
				{
					performBaseStrategy();
				}
			}
		}

		@Override
		public void setStrategy(IStrategy argStrategy) {
			if(strategy== null || !strategy.getStrategyName().equals(argStrategy.getStrategyName()))
			{
				strategy=argStrategy;
			}
		}
		
	}
	
	public class PeasantGroup extends BaseGroup implements IGroup
	{

		public PeasantGroup(int argGroupSize) {
			super(argGroupSize);
		}
		
	}
	public class KnightGroup extends BaseGroup implements IGroup
	{
		private ICastle homeCastle = null;
		private IStrategy defaultStrategy;
		public KnightGroup(int argGroupSize, ICastle castle) {
			super(argGroupSize);
			homeCastle=castle;
			defaultStrategy = new CherryPick(getMyRuler(), homeCastle);
		}	
		
		public IStrategy getDefaultStrategy() {
			return defaultStrategy;
		}
		public void setDefaultStrategy(IStrategy defStrategy) 
		{
			if(defaultStrategy == null || !defaultStrategy.getStrategyName().equalsIgnoreCase(defStrategy.getStrategyName()))
			{
				defaultStrategy=defStrategy;
			}
			
		}

		@Override
		protected void performBaseStrategy() {
			defaultStrategy.runStrategy(this);
		}	
		
		public ICastle getHomeCastle()
		{
			return homeCastle;
		}

	}
	public Integer getCastleValue() 
	{
		return castleValue;
	}
	
	public class PositionTracker
	{
		private Position trkPosition;
		private int movesAtSame =0;
		private int startMove=0;
		private IObject trkdSubject;
		
		public PositionTracker(IObject subject)
		{
			super();
			trkdSubject = subject;
			startMove=World.getCurrentTurn();
		}
		
		public boolean isConcern()
		{
			int curTurn = World.getCurrentTurn();
			return movesAtSame==0 && (startMove + 5)<curTurn;
		}

		public Position getTrkPosition() {
			return trkPosition;
		}

		public void setTrkPosition(Position trkPosition) {
			this.trkPosition = trkPosition;
		}

		public int getMovesAtSame() {
			return movesAtSame;
		}

		public void setMovesAtSame(int movesAtSame) {
			this.movesAtSame = movesAtSame;
		}
		
		public void addMove()
		{
			movesAtSame++;
		}

		public IObject getTrkdSubject() {
			return trkdSubject;
		}

		public void setTrkdSubject(IObject trkdSubject) {
			this.trkdSubject = trkdSubject;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + movesAtSame;
			result = prime * result
					+ ((trkPosition == null) ? 0 : trkPosition.hashCode());
			result = prime * result
					+ ((trkdSubject == null) ? 0 : trkdSubject.getId());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final PositionTracker other = (PositionTracker) obj;
			if (movesAtSame != other.movesAtSame)
				return false;
			if (trkPosition == null) {
				if (other.trkPosition != null)
					return false;
			} else if (!trkPosition.equals(other.trkPosition))
				return false;
			if (trkdSubject == null) 
			{
				if (other.trkdSubject != null)
					return false;
			} else if (trkdSubject.getId()!=other.trkdSubject.getId())
				return false;
			return true;
		}
		
	}
}
