#  Code Ruler

Back in the day when I was just learning Java, and coming from a COBOL background.  My mentor Steve Shaw challenged me to a competition.  He sent me a link to this game called CodeRuler, where you learned how to write Java by competing against other "bots" in battle.

Needles to say, I took it too far and created the best CodeRuler to have been created.  

How do I know this?  
- Well I scoured the internet for different versions of CodeRuler apps written by people
- Then I figured out how to turn the boring IBM bots into the bots from the code that I found
- I ran my CodeRuler against the top 3 that I found and still came out on top..
- My CodeRuler was so efficient, that I decided to code in a victory lap for my knights and peasants!

Here is some details
[Description](https://www.cse.lehigh.edu/~munoz/CSE497/assignments/files/coderuler.htm)

## Conquer medieval kingdoms with CodeRuler

**Stretch your Java programming skills with this new animated graphical simulator**


Guard your castle! Claim your land! Command your knights to joust valiantly and defeat their foes. Capture the enemy's position and seize its land while dodging its menacing knights. If writing mudane Java code is giving you the blues lately, maybe it's time to turn your medieval fantasies into reality. You can rule your own kingdom while refining your Java programming skills and mastering the Eclipse development environment all at the same time. It's all in a hard day's work for a supreme CodeRuler. Simulation-gaming enthusiast Sing Li puts you on the fast track to ultimate kingdom domination.
Born of the 2004 ACM International Collegiate Programming Competition (see Resources), CodeRuler is IBM alphaWorks' newest fantasy gaming simulator challenge. The game has a simple premise: You are the imperial ruler of your very own medieval kingdom. Your peasants and knights depend on your brilliant strategic thinking, agile adaptability, and superior Java programming skill to survive, increase, and prosper. Your objective as a player is to write Java code that simulates this ruler. The gaming simulator pits your ruler against up to six opponents' rulers (or the included sample rulers) and determines the winner.

This article guides you along the shortest path to ruling your own medieval kingdom. It reveals the game's environment, describes the rules, discusses general strategies, and provides two complete working ruler entries that you can put to use (or modify) immediately.

The simulation environment
CodeRuler is a graphical, animated simulation gaming environment. As a medieval ruler, you must battle other rulers for land and dominance. Your kingdom consists of:

Peasants who can claim and work the land.
Knights who can fight battles and capture other rulers' peasants, knights, or castles.
A castle that can create more knights and peasants. The more land you have, the faster it creates them.
The graphical gaming world
The game is played out in a two-dimensional world represented by a map of the kingdom. (The background landscape sketch merely acts as wallpaper; it doesn't affect game play or change as the game progresses.) 
![Alt text](image.png)
Figure 1 illustrates a CodeRuler game in progress
